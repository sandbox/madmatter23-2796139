<?php

namespace Drupal\licensing;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for license.
 */
class LicenseTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
